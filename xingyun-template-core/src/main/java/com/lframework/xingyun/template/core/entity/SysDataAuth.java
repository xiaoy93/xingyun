package com.lframework.xingyun.template.core.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 数据权限数据
 * </p>
 *
 * @author xy
 * @since 2024-03-19
 */
@Getter
@Setter
@TableName("sys_data_auth")
public class SysDataAuth implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId("id")
    private String id;

    /**
     * 角色代码
     */
    @TableField("role_code")
    private String roleCode;

    /**
     * 权限模型代码
     */
    @TableField("permission_code")
    private String permissionCode;

    /**
     * 菜单权限
     */
    @TableField("menu_permission")
    private String menuPermission;

    /**
     * 权限对象值
     */
    @TableField("value")
    private String value;

    /**
     * 创建人
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 创建人ID
     */
    @TableField("create_by_id")
    private String createById;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 修改人
     */
    @TableField("update_by")
    private String updateBy;

    /**
     * 修改人ID
     */
    @TableField("update_by_id")
    private String updateById;

    /**
     * 修改时间
     */
    @TableField("update_time")
    private Date updateTime;
}
