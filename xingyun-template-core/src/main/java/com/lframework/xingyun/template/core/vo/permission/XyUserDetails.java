package com.lframework.xingyun.template.core.vo.permission;

import com.lframework.starter.web.common.security.AbstractUserDetails;
import com.lframework.xingyun.template.core.entity.SysDataAuth;
import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author xiaoyan
 * @description: 权限模型扩展
 * @date 2024/3/19 14:40
 * @version: 1.0
 */
@Data
public class XyUserDetails extends AbstractUserDetails {

    private Map<String, List<SysDataAuth>> permissionsDataAuth;
}
