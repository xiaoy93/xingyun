package com.lframework.xingyun.template.core.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lframework.xingyun.template.core.entity.SysDataAuthModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 数据权限模型 Mapper 接口
 * </p>
 *
 * @author xy
 * @since 2024-03-19
 */
@Mapper
public interface SysDataAuthModelMapper extends BaseMapper<SysDataAuthModel> {

}
