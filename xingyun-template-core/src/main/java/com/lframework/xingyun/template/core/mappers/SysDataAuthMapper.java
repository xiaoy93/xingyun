package com.lframework.xingyun.template.core.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lframework.xingyun.template.core.entity.SysDataAuth;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 数据权限数据 Mapper 接口
 * </p>
 *
 * @author xy
 * @since 2024-03-19
 */
@Mapper
public interface SysDataAuthMapper extends BaseMapper<SysDataAuth> {

}
