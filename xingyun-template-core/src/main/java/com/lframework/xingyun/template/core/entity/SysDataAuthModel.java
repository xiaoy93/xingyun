package com.lframework.xingyun.template.core.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 * 数据权限模型
 * </p>
 *
 * @author xy
 * @since 2024-03-19
 */
@Getter
@Setter
@TableName("sys_data_auth_model")
public class SysDataAuthModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId("id")
    private String id;

    /**
     * 权限模型代码
     */
    @TableField("code")
    private String code;

    /**
     * 权限模型名称
     */
    @TableField("name")
    private String name;

    /**
     * 权限模型描述
     */
    @TableField("description")
    private String description;

    /**
     * 权限字段
     */
    @TableField("column_name")
    private String columnName;

    /**
     * 权限符号
     */
    @TableField("opt")
    private String opt;

    /**
     * 字典值，为空不取字典
     */
    @TableField("dic_id")
    private String dicId;

    /**
     * 权限默认值
     */
    @TableField("default_value")
    private String defaultValue;
}
