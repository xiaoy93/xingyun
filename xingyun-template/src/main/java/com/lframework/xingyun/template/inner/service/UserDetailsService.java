package com.lframework.xingyun.template.inner.service;

import com.lframework.starter.common.exceptions.impl.UserLoginException;
import com.lframework.starter.web.common.security.AbstractUserDetails;
import com.lframework.xingyun.template.core.vo.permission.XyUserDetails;

public interface UserDetailsService {

  /**
   * 根据用户名查询 用于登录认证
   *
   * @param username
   * @return
   * @throws UserLoginException
   */
  XyUserDetails loadUserByUsername(String username) throws UserLoginException;
}
